FROM strapi/base

WORKDIR /ps5-backend

COPY ./package.json ./
COPY ./package-lock.json ./

RUN npm install

COPY . .

ENV NODE_ENV production

RUN npm run build

EXPOSE 1337
EXPOSE 80

CMD ["npm", "start"]