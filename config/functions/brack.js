const axios = require("axios");
const HTMLParser = require("node-html-parser");

module.exports = async () => {
  const { data } = await axios.get(
    "https://www.brack.ch/fr/sony-console-de-jeu-playstation-5-1088594"
  );

  const page = HTMLParser.parse(data);

  const stock = page.querySelector(".stock__amount").innerText;
  const totalOrdered = page.querySelector(".stock__pendingOrder").innerText;

  var stock_detail = [];
  const raw_stock_detail = page.querySelector(".stock__detail").childNodes;
  raw_stock_detail.forEach((child) => {
    if (child.querySelector(".stock")) {
      var order = {
        items: child.querySelector(".stock").firstChild.innerText,
        date: child.querySelector(".stock").nextSibling.innerText,
      };
      stock_detail.push(order);
    }
  });

  let prevInfo = (
    await strapi.query("brack").find({ _limit: 1, _sort: "fetchedAt:desc" })
  )[0];

  let hasChanged = false;
  if (
    prevInfo.totalOrdered != totalOrdered ||
    prevInfo.stockDetails[0].date != stock_detail[0].date
  )
    hasChanged = true;

  if (hasChanged) {
    // send an email by using the email plugin
    await strapi.plugins["email"].services.email.send({
      to: ["poirier.alexis@gmail.com", "davidmiserez@gmail.com"],
      subject: "A nous la PS5 : Changement pour Brack",
      html: `
          <p style="font-size:2em"><b>OMG OMG OMG Il y a du nouveau !</b></p>
          <br/>
          <a href="https://www.brack.ch/fr/sony-console-de-jeu-playstation-5-1088594" target="_blank">Voir la page</a>
          <p>Avant:</p>
          <ul>
            <li>Stock : ${prevInfo.stock}</li>
            <li>Commandé : ${prevInfo.totalOrdered}</li>
            <li>Détail des commandes :
              <ul>
                <li>${prevInfo.stockDetails[0].items} : ${prevInfo.stockDetails[0].date}</li>
                <li>${prevInfo.stockDetails[1].items} : ${prevInfo.stockDetails[1].date}</li>
                <li>${prevInfo.stockDetails[2].items} : ${prevInfo.stockDetails[2].date}</li>
              </ul>
            </li>
          </ul>
          <br/>

          <p>Après:</p>
          <ul>
            <li>Stock : ${stock}</li>
            <li>Commandé : ${totalOrdered}</li>
            <li>Détail des commandes :
              <ul>
                <li>${stock_detail[0].items} : ${stock_detail[0].date}</li>
                <li>${stock_detail[1].items} : ${stock_detail[1].date}</li>
                <li>${stock_detail[2].items} : ${stock_detail[2].date}</li>
              </ul>
            </li>
          </ul>
        `,
    });
  }

  await strapi.query("brack").create({
    source_code: page.innerHTML,
    totalOrdered: totalOrdered,
    stock: stock,
    stockDetails: stock_detail,
    hasChanged: hasChanged,
    fetchedAt: new Date(),
  });
};
