const axios = require("axios");
const HTMLParser = require("node-html-parser");

module.exports = async () => {
  const { data } = await axios.get(
    "https://www.digitec.ch/fr/s1/product/sony-playstation-5-de-fr-it-en-consoles-de-jeu-12664145"
  );

  const page = HTMLParser.parse(data);

  const orderButton = page.querySelector("#addToCartButton");

  var buttonStatus = "";
  if ("disabled" in orderButton.rawAttributes) buttonStatus = "disabled";
  else buttonStatus = "enabled";

  const availabilityDate = page.querySelector(".availabilityText").innerText;

  let prevInfo = (
    await strapi.query("digitec").find({ _limit: 1, _sort: "fetchedAt:desc" })
  )[0];

  let hasChanged = false;
  if (prevInfo.buttonStatus != buttonStatus) hasChanged = true;

  if (hasChanged) {
    // send an email by using the email plugin
    await strapi.plugins["email"].services.email.send({
      to: ["poirier.alexis@gmail.com", "davidmiserez@gmail.com"],

      subject: "A nous la PS5 : Changement pour Digitec",
      html: `
            <p style="font-size:2em"><b>OMG OMG OMG Il y a du nouveau !</b></p>
            <br/>
            <a href="https://www.digitec.ch/fr/s1/product/sony-playstation-5-de-fr-it-en-consoles-de-jeu-12664145" target="_blank">Voir la page</a>
            <p>Avant:</p>
            <ul>
              <li>Statut du bouton : ${prevInfo.buttonStatus}</li>
              <li>Date de livraison : ${prevInfo.availabilityDate}</li>
            </ul>
            <br/>
  
            <p>Après:</p>
            <ul>
              <li>Statut du bouton : ${buttonStatus}</li>
              <li>Date de livraison : ${availabilityDate}</li>
            </ul>
          `,
    });
  }
  await strapi.query("digitec").create({
    source_code: page.innerHTML,
    buttonStatus: buttonStatus,
    availabilityDate: availabilityDate,
    hasChanged: hasChanged,
    fetchedAt: new Date(),
  });
};
