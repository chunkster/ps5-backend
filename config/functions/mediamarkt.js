const axios = require("axios");
const HTMLParser = require("node-html-parser");

module.exports = async () => {
  const { data } = await axios.get(
    "https://www.mediamarkt.ch/fr/product/_sony-ps-playstation-5-2018096.html"
  );

  const page = HTMLParser.parse(data);

  const availabilityMetaTag = page
    .querySelector(".box.infobox.availability")
    .querySelector("meta").rawAttributes.content;

  let prevInfo = (
    await strapi
      .query("mediamarkt")
      .find({ _limit: 1, _sort: "fetchedAt:desc" })
  )[0];

  let hasChanged = false;
  if (prevInfo.availabilityMetaTag != availabilityMetaTag) hasChanged = true;

  if (hasChanged) {
    // send an email by using the email plugin
    await strapi.plugins["email"].services.email.send({
      to: ["poirier.alexis@gmail.com", "davidmiserez@gmail.com"],

      subject: "A nous la PS5 : Changement pour Mediamarkt",
      html: `
            <p style="font-size:2em"><b>OMG OMG OMG Il y a du nouveau !</b></p>
            <br/>
            <a href="https://www.mediamarkt.ch/fr/product/_sony-ps-playstation-5-2018096.html" target="_blank">Voir la page</a>
            <p>Avant:</p>
            <ul>
              <li>Meta tag : ${prevInfo.availabilityMetaTag}</li>
            </ul>
            <br/>
  
            <p>Après:</p>
            <ul>
              <li>Meta Tag : ${availabilityMetaTag}</li>
            </ul>
          `,
    });
  }

  await strapi.query("mediamarkt").create({
    source_code: page.innerHTML,
    availabilityMetaTag: availabilityMetaTag,
    hasChanged: hasChanged,
    fetchedAt: new Date(),
  });
};
