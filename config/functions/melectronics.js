const axios = require("axios");
const HTMLParser = require("node-html-parser");

module.exports = async () => {
  const { data } = await axios.get(
    "https://www.melectronics.ch/fr/p/785445700000/sony-playstation-5"
  );

  const page = HTMLParser.parse(data);

  const orderButton = page.querySelector("#test-detail-btn-order");

  var buttonStatus = "";
  if ("disabled" in orderButton.rawAttributes) buttonStatus = "disabled";
  else buttonStatus = "enabled";

  const availabilityDate = page.querySelector(".buybox--annotation-text")
    .innerText;

  let prevInfo = (
    await strapi
      .query("melectronic")
      .find({ _limit: 1, _sort: "fetchedAt:desc" })
  )[0];

  let hasChanged = false;
  if (
    prevInfo.buttonStatus != buttonStatus ||
    prevInfo.availabilityDate != availabilityDate
  )
    hasChanged = true;

  if (hasChanged) {
    // send an email by using the email plugin
    await strapi.plugins["email"].services.email.send({
      to: ["poirier.alexis@gmail.com", "davidmiserez@gmail.com"],

      subject: "A nous la PS5 : Changement pour Melectronics",
      html: `
              <p style="font-size:2em"><b>OMG OMG OMG Il y a du nouveau !</b></p>
              <br/>
              <a href="https://www.melectronics.ch/fr/p/785445700000/sony-playstation-5" target="_blank">Voir la page</a>
              <p>Avant:</p>
              <ul>
                <li>Statut du bouton : ${prevInfo.buttonStatus}</li>
                <li>Date de livraison : ${prevInfo.availabilityDate}</li>
              </ul>
              <br/>
    
              <p>Après:</p>
              <ul>
                <li>Statut du bouton : ${buttonStatus}</li>
                <li>Date de livraison : ${availabilityDate}</li>
              </ul>
            `,
    });
  }

  await strapi.query("melectronic").create({
    source_code: page.innerHTML,
    buttonStatus: buttonStatus,
    availabilityDate: availabilityDate,
    hasChanged: hasChanged,
    fetchedAt: new Date(),
  });
};
