const axios = require("axios");
const HTMLParser = require("node-html-parser");

module.exports = async () => {
  const { data } = await axios.get(
    "https://www.microspot.ch/fr/search?search=9395201"
  );

  const page = HTMLParser.parse(data).toString();

  var notFound = false;
  var found = false;

  if (page.includes("aucun résultat")) notFound = true;
  if (page.includes("trouvés")) found = true;

  let prevInfo = (
    await strapi.query("microspot").find({ _limit: 1, _sort: "fetchedAt:desc" })
  )[0];

  let hasChanged = false;
  if (prevInfo.notFound != notFound || prevInfo.found != found)
    hasChanged = true;

  if (hasChanged) {
    // send an email by using the email plugin
    await strapi.plugins["email"].services.email.send({
      to: ["poirier.alexis@gmail.com", "davidmiserez@gmail.com"],

      subject: "A nous la PS5 : Changement pour Microspot",
      html: `
        <p style="font-size:2em"><b>OMG OMG OMG Il y a du nouveau !</b></p>
        <br/>
        <a href="https://www.microspot.ch/fr/search?search=9395201" target="_blank">Voir la page</a>
        <p>Avant:</p>
        <ul>
          <li>Mot-clé "aucun résultat" : ${prevInfo.notFound}</li>
          <li>Mot-clé "trouvé" : ${prevInfo.found}</li>
        </ul>
        <br/>

        <p>Après:</p>
        <ul>
          <li>Mot-clé "aucun résultat" : ${notFound}</li>
          <li>Mot-clé "trouvé" : ${found}</li>
        </ul>
      `,
    });
  }

  await strapi.query("microspot").create({
    source_code: page,
    notFound: notFound,
    found: found,
    hasChanged: hasChanged,
    fetchedAt: new Date(),
  });
};
