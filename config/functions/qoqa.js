const axios = require("axios");
const HTMLParser = require("node-html-parser");

module.exports = async () => {
  const { data, status, statusText } = await axios.get(
    "https://www.qoqa.ch/fr"
  );
  console.log(status + " : " + statusText);

  const offer = data.offer;
  var availability = false;

  if (!offer.is_sold_out) availability = true;

  if (availability) {
    // send an email by using the email plugin
    await strapi.plugins["email"].services.email.send({
      to: ["poirier.alexis@gmail.com", "davidmiserez@gmail.com"],

      subject: "A nous le 5*",
      html: `
              <p style="font-size:2em"><b>OMG OMG OMG Sexy time dans un hotel 5* incoming !</b></p>
              <br/>
              <a href="https://www.qoqa.ch/fr" target="_blank">Voir la page</a>
              <p>C'EST DISPO !!!!!!!!!!!!!!!! </p>
            `,
    });
  }

  await strapi.query("qoqa").create({
    available: availability,
  });
};
