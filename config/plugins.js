module.exports = ({ env }) => ({
  email: {
    provider: "nodemailer",
    providerOptions: {
      service: "gmail",
      auth: {
        user: env("GMAIL_USER"),
        pass: env("GMAIL_PASS"),
      },
    },
    settings: {
      defaultFrom: "fakaneblog@gmail.com",
      defaultReplyTo: "fakaneblog@gmail.com",
    },
  },
});
