module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 1337),
  admin: {
    auth: {
      secret: env("ADMIN_JWT_SECRET", "8a2c0b3103e35e07ab6311a001990800"),
    },
  },
  cron: {
    enabled: true,
  },
});
